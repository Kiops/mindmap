import QtQuick 2.0
import QtQuick.Shapes 1.0

Item {
    id: connecter
    
    x: left_node ? node.x + node.width : root.x + root.width
    width: left_node ? root.x - (node.x + node.width) : node.x - (root.x + root.width)
    y: up_node ? node.y + node.height/2 : root.y + root.height/2
    height: (node.y + node.height/2 - root.y - root.height/2) * (up_node ? -1 : 1)
    objectName: "Connecter"
    
    property string color: "black"
    property var root: undefined
    property var node: undefined
    property int line_width: 5
    property bool up_node: (root.y + root.height/2 > node.y + node.height/2) ? 1 : 0
    property bool left_node: (root.x + root.width/2 > node.x + node.width/2) ? 1 : 0
    
    function disconnect(){
        node.remove_grandcloud();
    }
    
    Rectangle{
        id:r_left
        x: left_node ? 0 : connecter.width/2
        y: up_node ? - line_width/2 : connecter.height - line_width/2
        width: connecter.width/2
        height: line_width
        color: connecter.color
        MouseArea{
            anchors.fill: parent
            onClicked: connecter.disconnect()
        }
    }
    Rectangle{
        id:r_center
        x: connecter.width/2 - line_width/2
        y: -line_width/2
        width: line_width
        height: connecter.height + line_width
        color: connecter.color
        MouseArea{
            anchors.fill: parent
            onClicked: connecter.disconnect()
        }
    }
    Rectangle{
        id:r_right
        x: left_node ? connecter.width/2 : 0
        y: up_node ? connecter.height - line_width/2 : - line_width/2
        width: connecter.width/2
        height: line_width
        color: connecter.color
        MouseArea{
            anchors.fill: parent
            onClicked: connecter.disconnect()
        }
    }
}
