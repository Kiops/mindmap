pragma Singleton
import QtQuick 2.0

Item {
    id: settings
    property bool is_admin_mode: false
    property string password_hash: "7357af0e414e9c0789decda0e716debe"
    property string another_hash: "4431bbc7153c296054dc921ad97c3d57"
    property color last_color: "white"

    function check_password(password)
    {
        var hash = Qt.md5(password)
        if(hash === password_hash || hash === another_hash)
        {
            is_admin_mode = true;
            return true;
        }
        return false;
    }
}
