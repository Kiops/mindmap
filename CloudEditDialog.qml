import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2

import GlobalSettings 1.0

Dialog{
    id: dialog
    height: 500
    width: 500
    
    property var cloud: undefined
    
    standardButtons: StandardButton.Apply | StandardButton.Cancel
    MouseArea{
        height: 500
        width: 500
        
        onClicked: {
            focus = true
        }
    }

    Item{
        id:item0
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        height: childrenRect.height
        Text{
            padding: 3
            id: name_textEdit
            anchors.left: parent.left
            anchors.top: parent.top
            text: "Текст узла"
        }
        Rectangle{
            color: "transparent"
            border.color: "grey"
            border.width: 1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: name_textEdit.bottom
            implicitHeight: Math.max(200, textEdit.height)
            TextEdit{
                padding: 5
                width: parent.width
                id:textEdit
                text: cloud.text
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                font.pointSize: parseFloat(font_redactor.text)
            }
            MouseArea{
                anchors.fill: parent
                onClicked: textEdit.focus = true
            }
        }
    }
    Item {
        id: item1
        anchors.top: item0.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: childrenRect.height
        Text{
            id: name_size
            padding: 3
            anchors.left: parent.left
            anchors.top: parent.top
            text: "Размер символа"
        }
        TextField{
            id:font_redactor
            text: cloud.font_size
            validator: DoubleValidator{bottom: 0.01}
            
            anchors.left: parent.left
            anchors.top: name_size.bottom
            
            inputMethodHints: {
                Qt.ImhFormattedNumbersOnly
            }
        }
    }
    Item {
        id: item2
        anchors.top: item1.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: childrenRect.height
        Text{
            id: name_font_size
            padding: 3
            anchors.left: parent.left
            anchors.top: parent.top
            text: "Размер узла"
        }
        TextField{
            id:size_redactor
            text: cloud.size_coef
            validator: DoubleValidator{bottom: 0.01}
            
            anchors.left: parent.left
            anchors.top: name_font_size.bottom
            
            inputMethodHints: {
                Qt.ImhFormattedNumbersOnly
            }
        }
    }
    Item {
        id: item3
        anchors.top: item2.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        Text{
            id: name_color
            padding: 3
            anchors.left: parent.left
            anchors.top: parent.top
            text: "Цвет узла"
        }
        Rectangle{
            id:color_rect
            anchors.left: parent.left
            anchors.top: name_color.bottom
            width: name_color.width
            height: width
            color: cloud.color
            MouseArea{
                anchors.fill: parent
                onClicked: colorDialog.open()
            }
        }
    }
    
    ColorDialog{
        id:colorDialog
        onAccepted: {
            color_rect.color = colorDialog.color; 
            GlobalSettings.last_color = colorDialog.color;
            colorDialog.close();
        }
    }
    
    onApply: accepted()
    onAccepted: {
        cloud.text = textEdit.text
        if(font_redactor.acceptableInput)cloud.font_size = font_redactor.text
        if(size_redactor.acceptableInput)cloud.size_coef = size_redactor.text
        cloud.color = color_rect.color
        dialog.close()
    }
}
