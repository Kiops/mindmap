import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2

import GlobalSettings 1.0

ApplicationWindow {
    //    visibility: "Maximized"
    id: applicationWindow
    visible: true
    width: 800
    height: 800
    title: qsTr("Mindmap")
    Item{
        anchors.fill: parent
        focus: true
        
        Playground{
            id: playground
            anchors.fill: parent
            BusyIndicator{
                id: busy
                running: true
                anchors.centerIn: parent
            }
            
            Component.onCompleted:{
                busy.running = false
            }
        }
        
        
        
        //Main keys catching
        Keys.onPressed: {
            if (event.key === Qt.Key_W && event.modifiers === Qt.ControlModifier)
            {
                console.log("Exit was requested by user")
                Qt.quit()
            }
            if (event.key === Qt.Key_K && event.modifiers === Qt.ControlModifier)
            {
                if(GlobalSettings.is_admin_mode)
                    GlobalSettings.is_admin_mode = false;
                else
                    login_dialog.open();
            }
        }
        Dialog{
            id: login_dialog
            standardButtons: StandardButton.Apply | StandardButton.Cancel
            TextField{
                id: textField
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                placeholderText: "password"
                echoMode: TextInput.Password
            }
            onVisibleChanged: {
                if(visible)
                    textField.focus = true
            }

            onApply: accepted()
            onAccepted: {
                var result = GlobalSettings.check_password(textField.text);
                textField.text = "";
                if(result === true)
                {
                    login_dialog.close();
                }
                else
                    login_dialog.open();
            }
            
        }
    }
}

