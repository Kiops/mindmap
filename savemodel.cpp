#include "savemodel.h"

SaveModel::SaveModel(QObject *parent) :
    QObject(parent)
{
    
}

QString SaveModel::write(QUrl filename){
    if(filename.isEmpty())
    {
        return QString("Error writing in file: filename is empty "
                       "file was not written");
    }
    if(clouds.isEmpty()){
        return QString("Error writing in file: vector of clouds is empty "
                       "file was not written");
        
    }
    QFile file(filename.toLocalFile());
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_10);
    in << clouds;
    file.flush();
    file.close();
    return QString("File" + filename.toString() + " was written successfully");
}

void SaveModel::read(QUrl filename)
{
    clear();
    QFile file(filename.toLocalFile());
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    in >> clouds;
    file.close();
//    for(int i = 0; i < clouds.size(); i++)
//    {
//        clouds[i].echo();
//    }
}

void SaveModel::clear()
{
    clouds.clear();
}

//принимает вектор QVariantmap, преобразует его в <Cloud> и записывает в SaveModel
void SaveModel::set_clouds(QVector<QVariantMap> qml_clouds){
    clear();
    for(auto i = 0; i < qml_clouds.size(); i++)
        clouds.push_back(Cloud(qml_clouds[i]));
}

//возвращает вектор QVariantmap, составленный на основе вектора clouds
QVector<QVariantMap> SaveModel::get_clouds()
{
    QVector<QVariantMap> qml_clouds;
    for(int i = 0; i < clouds.size(); i++)
        qml_clouds.push_back(clouds[i].get_map());
    return qml_clouds;
}

QVariantMap SaveModel::get_cloud(int index)
{
    return clouds[index].get_map();
}

int SaveModel::clouds_size(){
    return clouds.size();
}

void SaveModel::add_cloud (QVariantMap qml_cloud){
    clouds.push_back(Cloud(qml_cloud));
//    clouds.last().echo();
}
