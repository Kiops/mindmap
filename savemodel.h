#pragma once
#include <QtCore/QCoreApplication>
#include <QJSValue>
#include <QObject>
#include <QVector>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QHash>
#include <cloud.h>
#include <QUrl>


class SaveModel : public QObject
{
    Q_OBJECT
    

public:
    explicit SaveModel(QObject *parent = nullptr);
    
    QVector <Cloud> clouds;
    
//    Q_INVOKABLE void push_back(qreal area, qreal length, qreal max_strain,
//                               qreal youngs_modulus, qreal linear_load,
//                               qreal left_point_load, qreal right_point_load);
    
    Q_INVOKABLE QString write(QUrl filename);
    Q_INVOKABLE void read(QUrl filename);
    Q_INVOKABLE void set_clouds(QVector<QVariantMap> map);
    Q_INVOKABLE void add_cloud (QVariantMap map);
    Q_INVOKABLE QVector<QVariantMap> get_clouds();
    Q_INVOKABLE QVariantMap get_cloud(int index);
    Q_INVOKABLE int clouds_size();
    Q_INVOKABLE void clear();
    
private:
    
};
