import QtQuick 2.9

Item {

    Canvas {
        id: canvas

        property string color: "black"
        anchors.centerIn: parent

        height: parent.height / 2
        width: parent.width

        antialiasing: true

        onPaint: {
            var ctx = canvas.getContext('2d')

            ctx.strokeStyle = canvas.color
            ctx.lineWidth = canvas.height * 0.05
            ctx.beginPath()
            ctx.moveTo(canvas.width * 0.05, canvas.height)
            ctx.lineTo(canvas.width / 2, canvas.height * 0.1)
            ctx.lineTo(canvas.width * 0.95, canvas.height)
            ctx.stroke()
        }
    }
}
