
function check_solution(playground)
{
    var clouds = []
    for(var i = 0; i < playground.children.length; i++)
        if(playground.children[i].objectName === "Cloud"){
            clouds.push(playground.children[i])
        }
    var mistakes = 0;
    for(var i = 0; i < clouds.length; i++)
    {
        if(clouds[i].grandcloud !== undefined)
        {
            if(clouds[i].grandcloud.id_num === clouds[i].grandcloud_id_num)
            {
                clouds[i].glow("right");
                continue;
            }
        }
        else
        {
            if(clouds[i].grandcloud_id_num === -1)
            {
                clouds[i].glow("right");
                continue;
            }
        }
        mistakes++;
        clouds[i].glow("wrong")
    }
    if(mistakes > 0)
    {
        playground.text_for_popup = "Количество ошибок: " + mistakes;
    }
    else{
        console.log(playground.text_for_popup)
        playground.text_for_popup = "Ошибок нет"
    }
}

function glow_off(){
    for(var i = 0; i < playground.children.length; i++)
        if(playground.children[i].objectName === "Cloud"){
            playground.children[i].glow();
        }
}
