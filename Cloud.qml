import QtQuick 2.9
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

import GlobalSettings 1.0

Rectangle {
    id: cloud
    radius: 30 * size_coef
    visible: true
    border.width: 1
    border.color: "grey"
    objectName: "Cloud"
    property int id_num: 0
    property int default_size: 100
    property alias text: text_inner.text
    width: calcWidthOrHeight(shrink, text_inner.contentWidth, padding, size_coef, default_size)
    height: calcWidthOrHeight(shrink, text_inner.contentHeight, padding, size_coef, default_size)
    property int padding: 25 * size_coef
    property real size_coef: 1
    property bool ignore_drops: false
    property alias font_size: text_inner.font.pointSize
    property int subclouds_height: 0 //сумма высот сабклаудов
    property int height_including_subclouds: height //перезаписывается
    z: 5   
    
    property var subclouds: []
    property var grandcloud: undefined
    
    property bool shrink: false
    property bool right_from_grandcloud: false //находится справа от своего grandcloud
    
    property int offset_x: 40 //расстояние, на которое должны быть смещены subcloud'ы относительно своего grandcloud'a
    property int offset_y: 10 //расстояние между subcloud'ами одного grandcloud'a 
    
    property var grandcloud_connecter: undefined //коннектер к grandcloud'у
    property var subclouds_connecter: undefined //коннектр к сабклаудам
    property string connecter_text: '' //аннотация для connecter
    
    property int grandcloud_id_num: -1;
    property var subclouds_id_nums: []
    

    
    function calcWidthOrHeight(shr, cont, pad, size_c, standart)
    {
        return (shr ? standart : (cont + pad) < standart  ? standart : cont + pad) * size_c;
    }
    
    RectangularGlow {
        id: glower
        z: -1
        visible: false
        anchors.fill: parent
        glowRadius: 20
        spread: 0.2
        color: "green"
        cornerRadius: cloud.radius + glowRadius
    }
    
    function glow(param){
        if(param === undefined){
            glower.visible = false;
            return;
        }
        if(param === "right")
        {
            glower.color = "green";
        }
        else if(param === "wrong"){
            glower.color = "red";
        }
        glower.visible = true;
    }
    
    //сумма height_with_subclouds всех subclouds
    //независима от x
    function calculate_height_including_subclouds(){
        var sum = 0;
        if(subclouds.length > 0)
        {
            for(var i = 0; i < subclouds.length; i++)
            {
                subclouds[i].calculate_height_including_subclouds();
                sum += subclouds[i].height_including_subclouds;
            }
            sum += (offset_y * (subclouds.length-1))
        }
        subclouds_height = sum;
        height_including_subclouds = sum > height ? sum : height;
    }
    
    onHeightChanged: {
        if(grandcloud !== undefined)
        {
            grandcloud.rearrange_subclouds()
        }
        if(subclouds.length !== 0)
            rearrange_subclouds()
    }
    
    onWidthChanged: {
        if(grandcloud !== undefined)
        {
            grandcloud.rearrange_subclouds()
        }
        if(subclouds.length !== 0)
            rearrange_subclouds()
    }
    onHeight_including_subcloudsChanged: {
        if(grandcloud !== undefined)
            grandcloud.rearrange_subclouds()        
    }
    
    // Возвращает id subcloud'a, если находит его в subclouds. В противном случае возвращает -1
    function get_id_of_subcloud(subcloud)
    {
        for(var i = 0; i < subclouds.length; i++)
        {
            if(subclouds[i] === subcloud)
                return i;
        }
        return -1
    }
    
    //  Добавляет подоблако данному облаку
    function add_subcloud(subcloud)
    {
        //проверяем, если ли уже этот сабклауд у данного клауда
        if(get_id_of_subcloud(subcloud) !== -1)
            return 1;
        
        //добавляем объектные связи
        subcloud.grandcloud = cloud;
        var copy_subclouds = subclouds;
        copy_subclouds.push(subcloud);
        subclouds = copy_subclouds;
        
        //переориентируем сабклауды
        rearrange_subclouds();
        
        //подключаем к коннектеру 
        subclouds_connecter.connect_node(subcloud)
        subcloud.grandcloud_connecter = subclouds_connecter
        
        return 0;
    }
    
    
    //  Удаляет из sublouds данное подоблако
    function remove_subcloud(subcloud)
    {
        var victim = get_id_of_subcloud(subcloud);
        if(victim === -1)
        {
            console.log("Error: ", subcloud, "is not a subcloud of ", cloud, "and can't be removed from it")
            return 1;
        }
        subclouds.splice(victim, 1);
        subcloud.grandcloud = undefined;
        subcloud.grandcloud_connecter = undefined;
        subclouds_connecter.disconnect_node(subcloud)
        rearrange_subclouds();
        return 0;
    }
    
    function remove_grandcloud()
    {
        if(grandcloud === undefined)
            return 1;
        grandcloud.remove_subcloud(cloud);
        return 0;
    }
    
    function disconnect_all_subclouds(){
        for(var i = 0; i < subclouds.length; i++)
        {
            subclouds[i].grandcloud = undefined;
            subclouds[i].grandcloud_connecter = undefined;
            subclouds_connecter.disconnect_node(subclouds[i])
        }
        subclouds = []
    }
    
    function delete_me()
    {
        disconnect_all_subclouds()
        remove_grandcloud()
        offset_x = 0
        subclouds_connecter.destroy()
        cloud.destroy()
    }
    
    onXChanged: {
        rearrange_x()
    }
    
    onYChanged: {
        rearrange_y()
    }
    
    onOffset_xChanged: {
        rearrange_x()
    }
    
    function rearrange_x()
    {
        if(subclouds.length===0)
            return;
        calculate_height_including_subclouds()
        
        var first_of_kind = subclouds[0]
        first_of_kind.x = x + (right_from_grandcloud ? offset_x : -offset_x - first_of_kind.width);
        for(var i = 1; i < subclouds.length; i++)
        {
            var me = subclouds[i]
            var before_me = subclouds[i-1]
            me.x = x + (right_from_grandcloud ? offset_x : -offset_x - me.width);
        } 
    }
    
    function rearrange_y()
    {
        if(subclouds.length===0)
            return;
        calculate_height_including_subclouds()
        var ear_height = function (cl) {
            if(cl.subclouds_height<=cl.height)
                return 0;
            return (cl.subclouds_height - cl.height)/2;
        };
        
        var y_for_subcloud = y + height/2 - subclouds_height/2;
        y_for_subcloud += ear_height(subclouds[0])
        for(var i = 0; i < subclouds.length; i++)
        {
            subclouds[i].y = y_for_subcloud;
            y_for_subcloud += (subclouds[i].height + offset_y);
            y_for_subcloud += ear_height(subclouds[i])
            if(subclouds[i+1]!==undefined)
                y_for_subcloud += ear_height(subclouds[i+1])
        } 
    }
    
    //перенастраивает привязки по координатам своих subclouds
    function rearrange_subclouds()
    {
        rearrange_x()
        rearrange_y()
    }    
    
    function bind_with_connecter()
    {
        subclouds_connecter.root = cloud
        subclouds_connecter.offset_x = cloud.offset_x;
        offset_x = Qt.binding(function(){return subclouds_connecter ? subclouds_connecter.additionnal_offset + subclouds_connecter.offset_x : 0})
        subclouds_connecter.text = cloud.connecter_text ? cloud.connecter_text : ''
        cloud.connecter_text = Qt.binding(function(){return subclouds_connecter ? subclouds_connecter.text : 0})
    }
    
    
    //Drag and drop 
    Drag.active: dragArea.drag.active
    Drag.hotSpot: Qt.point(dragArea.mouseX, dragArea.mouseY)
    
    Item {
        id:contentWrapper
        width: cloud.shrink ? default_size : parent.width
        height: cloud.shrink ? default_size : parent.height
        anchors.centerIn: parent
        clip: true
        TextEdit{
            id: text_inner
            anchors.centerIn: parent
            activeFocusOnPress: false
            horizontalAlignment: TextEdit.AlignHCenter
            verticalAlignment: TextEdit.AlignVCenter
            text: qsTr("Node %1").arg(cloud.id_num)
            color: cloud.color.hslLightness > 0.5 ? "black" : "white"
            wrapMode: shrink ? TextEdit.WrapAnywhere : TextEdit.NoWrap
        }
    }
    
    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: parent

        acceptedButtons: Qt.LeftButton | Qt.RightButton

        onClicked: {

            if(mouse.button === Qt.MiddleButton)
                cloud.remove_grandcloud()
            if(mouse.button === Qt.RightButton)
                contextMenu.popup()
        }
        
        drag.onActiveChanged: {
            if(drag.active){
                text_inner.focus = false;
                dragActive(cloud);
            }
            
            if(cloud.grandcloud !== undefined)
                if(drag.active)
                    cloud.grandcloud_connecter.disconnect_node(cloud)
                else
                {
                    if(!cloud.grandcloud_connecter.is_connected(cloud))
                    {
                        cloud.grandcloud_connecter.connect_node(cloud)
                    }
                    cloud.grandcloud.rearrange_subclouds()
                }
        }
        
        onPressed: {
            dropArea.enabled = false; 
            parent.z += 1; 
        }
        
        onReleased: {
            parent.Drag.drop(); 
            dropArea.enabled = true;
            parent.z -=1;
        }
        
        
        Menu {
            id: contextMenu
            MenuItem { text: "Отсоединить от родителя"; onClicked: cloud.remove_grandcloud() }
            MenuItem { text: "Отсоединить всех наследников"; onClicked: cloud.disconnect_all_subclouds()}
            MenuItem { 
                text: "Редактировать"
                visible: GlobalSettings.is_admin_mode
                height: visible ? implicitHeight : 0
                onClicked: cloudEditDialog.open()
            }
            MenuItem { 
                text: "Удалить"
                visible: GlobalSettings.is_admin_mode
                height: visible ? implicitHeight : 0
                onClicked: delete_me()
            }
//            MenuItem { text: shrink ? "Deshrink" : "Shrink"; onClicked:  shrink = !shrink}
        }
    } 
    
    CloudEditDialog{
        id:cloudEditDialog
        cloud: cloud
    }
    
    DropArea {
        id: dropArea  
        anchors.fill: parent
        
        onDropped: {
            if(drag.source !== parent){
                var subcloud = drag.source
                if(subcloud.grandcloud !== undefined)
                    subcloud.remove_grandcloud()
                cloud.add_subcloud(drag.source)
            }
        } 
    }
    
    
    signal dragActive(var me);
    
    Component.onCompleted: {
        cloud.color = GlobalSettings.last_color;
        bind_with_connecter();
    }
}
