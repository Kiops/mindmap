#include "cloud.h"
#include <QDebug>

Cloud::Cloud()
{
    grandcloud_id_num = -1; //нет родителя
}

//qint32 x, y, id_num, grandcloud_id_num, offset_x, offset_y;
//QVector<qint32> subclouds_id_nums;
//QString text;
//QColor color;

QDataStream &operator <<(QDataStream &stream,const Cloud &cloud) // сериализуем;
{
    stream << cloud.x << cloud.y << cloud.id_num << cloud.grandcloud_id_num 
           << cloud.offset_x << cloud.offset_y << cloud.subclouds_id_nums 
           << cloud.text << cloud.color << cloud.connecter_text;
    return stream;
}

QDataStream &operator >>(QDataStream &stream, Cloud &cloud) // десериализуем;
{
    stream >> cloud.x >> cloud.y >> cloud.id_num >> cloud.grandcloud_id_num 
           >> cloud.offset_x >> cloud.offset_y >> cloud.subclouds_id_nums 
           >> cloud.text >> cloud.color >> cloud.connecter_text;
    return stream;
}

Cloud::Cloud(QVariantMap map){
    
    //преобразование List<Qvariant> к List<Qint>
    QList<QVariant> sinv= map["subclouds_id_nums"].toList();
    QList<qint32> subclouds_ids;
    for(auto i = 0; i < sinv.size(); i++)
    {
        subclouds_ids.append(sinv.at(i).toInt());
    }
    
    //заполнение данными из map
    x = map["x"].toInt();
    y = map["y"].toInt();
    id_num = map["id_num"].toInt();
    grandcloud_id_num = map["grandcloud_id_num"].toInt();
    offset_x = map["offset_x"].toInt();
    offset_y = map["offset_y"].toInt();
    subclouds_id_nums = subclouds_ids;
    text = map["text"].toString();
    color = map["color"].toString();
    connecter_text = map["connecter_text"].toString();
}

QVariantMap Cloud::get_map(){
    QList<QVariant> sinv;
    for(auto i = 0; i < subclouds_id_nums.size(); i++)
    {
        sinv.append(QVariant(subclouds_id_nums.at(i)));
    }
    
    QVariantMap map;
    map.insert("x", x);
    map.insert("y", y);
    map.insert("id_num", id_num);
    map.insert("grandcloud_id_num", grandcloud_id_num);
//    map.insert("offset_x", offset_x);
//    map.insert("offset_y", offset_y);
    map.insert("subclouds_id_nums", sinv);
    map.insert("text", text);
    map.insert("color", color);
    map.insert("connecter_text", connecter_text);
    return map;
}

void Cloud::echo(){
    qDebug() << "x: " << x;
    qDebug() << "y: " << y;
    qDebug() << "text: " << text;
    qDebug() << "id_num" << id_num;
    qDebug() << "grandcloud_id_num: " << grandcloud_id_num;
    qDebug() << "subclouds_id_nums: " << subclouds_id_nums;
}


