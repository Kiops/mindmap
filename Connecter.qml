import QtQuick 2.0
import QtQuick.Shapes 1.0

Item {
    id: connecter
    
    x: 0
    y: 0
    width: 0
    height: 0
    objectName: "Connecter"
    
    property string color: "black"
    property var root: {x = 0; y = 0}
    property var nodes: ([])
    property var sprites: ({})
    property int line_width: 2
    property alias text: textEdit.text
    property int offset_x: 100
    property int additionnal_offset: textEdit.width 
    visible: false
    
    function connect_node(node)
    {
        nodes.push(node);
        var node_rect = component.createObject(connecter);
        node_rect.color = connecter.color;
        node_rect.x = Qt.binding(function(){return node.x+node.width});
        node_rect.y = Qt.binding(function(){return node.y + node.height/2 - line_width/2})
        node_rect.height = Qt.binding(function(){return connecter.line_width})
        node_rect.width = Qt.binding(function(){return r_center.x - node_rect.x})
        
        //        console.log(node_rect.color, node_rect.x, node_rect.y, node_rect.height, node_rect.width)
        
        sprites[node] = node_rect;
        calculate_low_high()
        if(nodes.length > 0)
            visible = true
        //        console.log(nodes.length)
    }
    
    function disconnect_node(node)
    {
        calculate_low_high(node)
        for(var i = 0; i < nodes.length; i++)
        {
            if(nodes[i] === node)
            {
                nodes.splice(i,1)
                break
            }
        }
        //todo check what this for
        if(sprites[node])
            sprites[node].destroy()
        delete sprites[node]
        if(nodes.length === 0)
            visible = false;
    }
    
    function calculate_low_high(ignore_node)
    {
        var min = parent.height
        var max = 0
        var hm = undefined
        var lm = undefined
        for(var i = 0; i < nodes.length; i++)
        {
            if(nodes[i] === ignore_node)
                continue
            if(nodes[i].y + nodes[i].height/2 < min)
            {
                min = nodes[i].y + nodes[i].height/2
                hm = nodes[i]
            }
            if(nodes[i].y + nodes[i].height/2 > max)
            {
                max = nodes[i].y + nodes[i].height/2
                lm = nodes[i]
            }
        }
        if(root.y + root.height/2 > max)
            lm = root
        if(root.y + root.height/2 < min)
            hm = root
        r_center.highermost = hm
        r_center.lowermost = lm
    }
    
    function is_connected(node)
    {
        for(var i = 0; i < nodes.length; i++)
        {
            if(nodes[i] === node)
                return true
        }
        return false
    }
    
    
    Component{
        id: component
        Rectangle{
            onYChanged: {calculate_low_high()}
//            Arrow{
//                rotation: 270
//                anchors.left: parent.left
//                anchors.verticalCenter: parent.verticalCenter
//                height: parent.height * 3
//                width: 20
//            }
        }
    }
    
    Rectangle{
        id:r_center
        x: r_root.x - line_width
        property var highermost: undefined //самый верхний прямоугольник
        property var lowermost: undefined //самый нижний прямоугольник
        width: line_width
        color: connecter.color
        
        onHighermostChanged: {
            if(highermost !== undefined)
                y = Qt.binding(function(){return highermost.y + highermost.height/2 - line_width/2})
            else
                y = 0
        }
        onLowermostChanged: {
            if(lowermost !== undefined)
                height = Qt.binding(function(){return lowermost.y + lowermost.height/2 + line_width/2 - y})
            else height = 0
        }
        
        MouseArea{
            anchors.fill: parent
        }
        
        
    }
    
    Rectangle{
        id:r_root
        x: root.x - r_root.width
        y: root.y + root.height/2 - line_width/2
        width:  offset_x/2 -line_width/2 + additionnal_offset
        height: line_width
        color: connecter.color
        MouseArea{
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 10
            onClicked: textEdit.focus = true
        }
    }
    
    Item{
        id:wrapper
        width: textEdit.width
        height: textEdit.height + 2
        anchors.horizontalCenter: r_root.horizontalCenter
        anchors.bottom: r_root.top
        TextEdit{
            id: textEdit
        }
    }
    
}
