import QtQuick 2.9
import QtQuick.Dialogs 1.3
import QtQuick.Shapes 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import GlobalSettings 1.0
import "PlaygroundWatcher.js" as Watcher


Item {
    id: playground
    property int id_setter: 0
    property string text_for_popup: "Default text for popup"
    property var cloud_component : Qt.createComponent("Cloud.qml")
    property var connecter_component: Qt.createComponent("Connecter.qml")
    
    Popup{
        id: popup
        dim: true
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2
        Text{
            id: my_text
            font.pointSize: 15
            anchors.centerIn: parent
            text: playground.text_for_popup;
        }
        
    }
    
    MouseArea{
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onDoubleClicked: {
            if(GlobalSettings.is_admin_mode)
            {
                var cloud = create_cloud(mouseArea.mouseX, mouseArea.mouseY);
                cloud.x -= cloud.width/2;
                cloud.y -= cloud.height/2;
            }
        }
        
        property point savedMousePosition: Qt.point(0,0)
        
        onPressed: {
            savedMousePosition = Qt.point(mouseX, mouseY)
        }
        
        onSavedMousePositionChanged: {
            playground.x -= savedMousePosition.x - mouseX
            playground.y -= savedMousePosition.y - mouseY
        }
        
        onClicked: {
            focus = true
            if(mouse.button === Qt.RightButton)
                contextMenu.popup()
        }
        Menu {
            id: contextMenu
            MenuItem {
                visible: GlobalSettings.is_admin_mode
                height: visible ? implicitHeight : 0
                text: "Содать"
                onTriggered: {
                    var cloud = create_cloud(mouseArea.mouseX, mouseArea.mouseY);
                }
            }
            MenuItem{
                visible: GlobalSettings.is_admin_mode
                height: visible ? implicitHeight : 0
                text: "Сохранить"
                //                onClicked: saveModel.save_clouds("test");
                onTriggered: {fileDialog.selectExisting = false; fileDialog.open()}
            }
            MenuItem{
                text: "Загрузить"
                //                onClicked: saveModel.load_clouds("test")
                onTriggered: {fileDialog.selectExisting = true; fileDialog.open()}
            }
            MenuItem{
                text: "Очистить"
                onTriggered: playground.remove_clouds_and_connecters()
            }
            MenuItem{
                visible: !GlobalSettings.is_admin_mode
                height: visible ? implicitHeight : 0
                text: "Проверить решение"
                onTriggered: {Watcher.check_solution(playground); popup.open()}
            }
            //            MenuItem{
            //                text: "Отключить свечение"
            //                onTriggered: {Watcher.glow_off()}
            //            }
        }
    }
    
    FileDialog{
        id: fileDialog
        //@disable-check M16
        defaultSuffix: "kio"
        onAccepted: {
            if(selectExisting)
                if(GlobalSettings.is_admin_mode)
                    saveModel.load_clouds(fileDialog.fileUrl)
                else
                    saveModel.load_quest(fileDialog.fileUrl)
            else
                saveModel.save_clouds(fileDialog.fileUrl /*+ '.' + fileDialog.defaultSuffix*/)
        }
        nameFilters: [ "Mindmap saves(*.kio)", "All files (*)" ]
        
    }
    
    function create_cloud(x, y)
    {
        var sprite = cloud_component.createObject(playground, {
                                                      "x" : x, 
                                                      "y" : y, 
                                                      "id_num" : playground.id_setter, 
                                                      "subclouds_connecter" : connecter_component.createObject(playground)});
        playground.id_setter++;
        return sprite;
    }
    
    function remove_clouds_and_connecters()
    {
        for(var i = 0; i < children.length; i++)
            if(children[i].objectName === "Connecter")
                children[i].destroy();
        // @disable-check M107
        for(var i = 0; i < children.length; i++)
            if(children[i].objectName === "Cloud")
                children[i].destroy();
        playground.id_setter = 0;
    }
    
    SaveModel{
        id:saveModel
        playground: playground
    }
}
