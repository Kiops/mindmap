#pragma once
#include <QtCore/QCoreApplication>
#include <QDataStream>
#include <QVariantMap>
#include <QVector>
#include <QString>

class Cloud
{
public:
    qint32 x, y, id_num, grandcloud_id_num, offset_x, offset_y;
    QList<qint32> subclouds_id_nums;
    QString text;
    QString color;
    QString connecter_text;
    
    friend QDataStream &operator <<(QDataStream &stream,const Cloud &cloud);
    friend QDataStream &operator >>(QDataStream &stream, Cloud &cloud);
    
    Cloud();
    Cloud(QVariantMap);
    QVariantMap get_map();
    void echo(); // выводит состояние клауда
};
