import KiopsQML 1.1


SaveModel{
    id: saveModel
    property var playground: undefined
    
    //записывает текущее состояние облаков в файл
    function save_clouds(filename){
        saveModel.clear()
        var children = playground.children;
        for(var i = 0; i < children.length; i++){
            if(children[i].objectName === "Cloud"){
                var cloud = {
                    "x" : children[i].x,
                    "y" : children[i].y,
                    "id_num" : children[i].id_num,
                    "grandcloud_id_num" : children[i].grandcloud !== undefined ? children[i].grandcloud.id_num : -1,
                                                                                 "offset_x" : children[i].offset_x,
                                                                                 "offset_y" : children[i].offset_y,
                                                                                 "subclouds_id_nums" : get_subclouds_id_nums(children[i].subclouds),
                                                                                 "text" : children[i].text,
                                                                                 "color" : children[i].color,
                                                                                 "connecter_text" : children[i].connecter_text
                }
//                console.log(i)
                saveModel.add_cloud(cloud);
            }
        }
        saveModel.write(filename)
    }
    
    function load_clouds(filename){
        read(filename);
        var clouds = {}
        playground.remove_clouds_and_connecters()
        for(var i = 0; i < clouds_size(); i++)
        {
            var cloud = get_cloud(i);
            var sprite = playground.create_cloud();
            set_values(sprite, cloud);
            clouds[sprite.id_num] = sprite;
        }
        // @disable-check M107
        for(var cloud in clouds){
            if(clouds[cloud].grandcloud_id_num !== -1)
                clouds[clouds[cloud].grandcloud_id_num].add_subcloud(clouds[cloud]);
        }
    }
    
    function load_quest(filename){
        read(filename);
        var clouds = {}
        playground.remove_clouds_and_connecters()
        for(var i = 0; i < clouds_size(); i++)
        {
            var cloud = get_cloud(i);
            var sprite = playground.create_cloud();
            set_values(sprite, cloud);
            place_on_a_randon_position(sprite)
            clouds[sprite.id_num] = sprite;
        }
    }
    
    function place_on_a_randon_position(cloud)
    {
        cloud.x = Math.floor(Math.random() * (playground.width - cloud.width));
        cloud.y = Math.floor(Math.random() * (playground.height - cloud.height));
    }
    
    //вынести в отдельный js
    function get_subclouds_id_nums(subclouds){
        var result = [];
        for(var i = 0; i < subclouds.length; i++)
        {
            result.push(subclouds[i].id_num);
        }
        return result;
    }
    
    function set_values(sprite, cloud)
    {
        for (var key in cloud) {
            if(key in sprite)
                sprite[key] = cloud[key];
        }
        sprite.bind_with_connecter()
    }
    
}
